#include<stdio.h>

void ocurrencia(int *arreglo,int x, int tam){
	int *p=arreglo;
	printf("Este es el arreglo de numeros: {");
	for(int a=0;a<tam;a++){
		if(a==tam-1){
			printf("%i",p[a]);
		}else{
			printf("%i,",p[a]);
		}
	}
	printf("} El numero que usted desea buscar es:  %i\n",x);

	int contadorOcurrencias=0;
	for(int i=0;i<tam;i++){
		if(p[i]==x){
			printf("El elemento ha sido encontrado\n");
			printf("Elemento:%i\t La direccion en memoria es :%p\n",p[i],&p[i]);
			contadorOcurrencias++;
		}
	}
	if(contadorOcurrencias==0){
		printf("No se encontraron ocurrencias para el elemento que usted desea buscar. \n");
	}
}
int main(){
    int arreglo[10]={1,2,3,4,5,6,7,8,9,0};
    ocurrencia(arreglo,16,10);
    
}
