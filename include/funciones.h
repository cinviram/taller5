//tema1
void bytesInt(int valor);
void bytesLong(long valor);
void bytesFloat(float valor);
void bytesDouble(double valor);
//tema2
void datosURL(char *url);
//tema3
void ocurrencia(int *arreglo,int x, int tam);
//tema4
void imc(float peso, float altura, float **resp);
//tema5
void iva( struct producto* prod);
//tema6
void direcciones (struct producto *p[], int n[]);
