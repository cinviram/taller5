bin/rol:obj/main.o obj/tema1.o obj/tema2.o obj/tema3.o obj/tema4.o obj/tema5.o obj/tema6.o
        gcc -Wall $^ -o $@
obj/main.o: src/main.c
        gcc -Wall -Iinclude $< -c -o $@
obj/tema1.o: src/tema1.c
        gcc -Wall -Iinclude $< -c -o $@
obj/tema2.o: src/url.c
        gcc -Wall -Iinclude $< -c -o $@
obj/tema3.o: src/ocurrencias.c
        gcc -Wall -Iinclude $< -c -o $@
obj/tema4.o: src/ocurrencias.c
        gcc -Wall -Iinclude $< -c -o $@
obj/tema5.o: src/tema5.c
        gcc -Wall -Iinclude $< -c -o $@
obj/tema6.o: src/ejercicio6.c
        gcc -Wall -Iinclude $< -c -o $@
clean:
        rm obj/*o || rm bin/*
.PHONY: run
run:
        bin/rol

